package net.cereal.stagengine.ui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class SwingUI extends JFrame implements IGameUI {
	private static final long serialVersionUID = 1L;
	
	private String input;
	private boolean inputEnabled;

	private JTextArea jtaContent;
	private JTextField jtfInput;
	private JButton jbConfirm;
	private JLabel jbStatus;
	
	public SwingUI(String title, Font f) {
		super(title);
		
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }
		
		jtaContent = new JTextArea();
		jtaContent.setEditable(false);
		jtaContent.setFont(f);
		jtaContent.setLineWrap(true);
		jtaContent.setWrapStyleWord(true);
		
		jtfInput = new JTextField();
	}

	@Override
	public void append(String s) {
		jtaContent.append(s);
	}

	@Override
	public String getText() {
		return jtaContent.getText();
	}

	@Override
	public void setText(String s) {
		jtaContent.setText(s);
	}

	@Override
	public String getInput() {
		try {
			synchronized (input) {
		        while (input.isEmpty())
		        	input.wait();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return input;
	}

	@Override
	public void setInputEnabled(boolean enabled) {
		if(inputEnabled == enabled)
			return;
		inputEnabled = enabled;
		jtfInput.setEnabled(inputEnabled);
		jbConfirm.setEnabled(inputEnabled);
	}
	
	@Override
	public void setBackgroundColor(Color c) {
		
	}
	
	@Override
	public void setForegroundColor(Color c) {
		
	}

}
