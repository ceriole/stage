package net.cereal.stagengine.testgame;

import java.awt.Font;

import net.cereal.stagengine.ui.IGameUI;
import net.cereal.stagengine.ui.SwingUI;

public class Main {
	
	public static void main(String[] args) {
		IGameUI ui = new SwingUI("TestGame", new Font("Consolas", Font.PLAIN, 24));
		ui.append("Dank memes");
	}

}
